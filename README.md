# README #

This repository implements zipping and downloading attachments from a salesforce record.

### What is this repository for? ###

Use it to create a VF page that lists all the attachments from a record and its related records. The user can select multiple or all attachments and download them as a single zip.

### How do I get set up? ###

This is an example code. Modify as per need.
In the example, the code lists all the attachments from a record of the object Instrument__c and related parent Contract record (Method name -
 **getAttachments**).

Remote action method **getAttachment** returns one attachment at a time in a wrapper.

webservice method **checkAttachment** can be used to check if the record has any attachments to be downloaded. Use Case- call on click javascript on a button and call checkAttachment to show an error message if there aren't any attachments or move to the attachment list Visualforce page if there is at least one.

The visualforce page has all the javascript to zip and download. Uses the js libraries [filesaver](https://github.com/eligrey/FileSaver.js/) and [jszip](https://stuk.github.io/jszip/) 

### Limits ###
Due to the governor limits around and remote actions and size changes when converting an attachment to base64 encoded string, a file larger than 15mb after encoding will not be downloaded/zipped. An error message is shown.

There is no limit to size of zip, but to prevent memory crash on browser, limitation of only 200 attachments at a time has been put in. It can be removed. Check the javascript on the Visualforce page.

### Who do I talk to? ###
nagen.sahu@absyz.com