global with sharing class AttachmentViewPageController {
    
    private static String API_STATUS_NORMAL = '200';
    public String instrId{get;set;}
    public String contractId{get;set;}
    public List <AttachmentWrapper> lstAttachmentWrapper{get;set;}
    public String fileName {get;set;}
    public Map<Id,String> objMap{get;set;}
    
    public AttachmentViewPageController(ApexPages.StandardController controller) {
        instrId = ApexPages.currentpage().getparameters().get('recID');       
    }
     
    //Wrappimg the Attachement records    
    public List<AttachmentWrapper> getAttachments(){
        lstAttachmentWrapper = new List <AttachmentWrapper>();
        
        List<Attachment> lstAttachments = new List<Attachment>();
        List <AttachmentWrapper> lstAttachmentWrapper = new List <AttachmentWrapper>();
        
        AttachmentWrapper wrapper;
        
        Instrument__c instrumentObj = [Select Id,Name,Contract__r.Id From Instrument__c Where Id =: instrId];
        fileName = instrumentObj.Name;
        contractId = instrumentObj.Contract__r.Id;
        objMap = new Map<Id,String>{contractId => 'Contract', instrId => 'Instrument'};
        Set<Id> parentIdSet = new Set<Id>();
        parentIdSet.add(instrId);
        parentIdSet.add(contractId);
        
        
        /*//Adding Instrument Object to List
        for(Attachment attch:fetchInstrumentAttachments(instrId)){
            wrapper = new AttachmentWrapper();
            wrapper.objectType = 'Instrument';
            wrapper.attObj = attch;
            wrapper.attName = attch.Name;
           // wrapper.attEncodedBody = EncodingUtil.base64Encode(attch.Body);
            wrapper.isSelected = false;
            lstAttachmentWrapper.add(wrapper);
        }
        
        //Adding Contract Object to List
        for(Attachment att : fetchContractAttachments(contractId)){
            wrapper = new AttachmentWrapper();
            wrapper.objectType = 'Contract';
            wrapper.attObj = att;
            wrapper.attName = att.Name;
            //wrapper.attEncodedBody = EncodingUtil.base64Encode(att.Body);
            wrapper.isSelected = false;
            lstAttachmentWrapper.add(wrapper);
        }*/
       
        return fetchAllAttachments(parentIdSet);
    }
    
    // fetches the Instrument object Attachments
    /*public List<Attachment> fetchInstrumentAttachments(String instrumentId){
        Instrument__c instrumentObj = [Select Id,Name,Contract__r.Id From Instrument__c Where Id =: instrumentId];
        fileName = instrumentObj.Name;
        contractId = instrumentObj.Contract__r.Id;
        
        List<Attachment> lstAttachments = [SELECT Id, Name, ParentId, ContentType,BodyLength,owner.Name,createddate FROM Attachment WHERE ParentId =:instrumentObj.Id];
        
        return lstAttachments;
    } 
    
    // fetches the Contract object Attachments
    public List<Attachment> fetchContractAttachments(String contractId){
        Contract contractObj = [Select Id,Name From Contract Where Id =: contractId];
        List<Attachment> lstAttachments = [SELECT Id, Name, ParentId, ContentType,BodyLength , Body,owner.Name,createddate FROM Attachment WHERE ParentId =:contractObj.Id];
        
        return lstAttachments;
    }*/
    
    //this method called by custom button "Download Attachemt".
    webservice static Boolean checkAttachment(String instrId){
        Instrument__c instrumentObj = [Select Id,Name,Contract__r.Id From Instrument__c Where Id =: instrId];
        List<Attachment> lstAtt = [SELECT Id,Name from Attachment where ParentId =:instrId OR ParentId =: instrumentObj.Contract__r.Id];
        if(lstAtt.size()>0){
            return true;
        }
        else{
            return false;
        }
    }
    
    @RemoteAction
    public static AttachmentWrapper getAttachment(String attachmentId){
       AttachmentWrapper attwrap = new AttachmentWrapper();
       attachment attchmnt = new attachment();
        attchmnt = [select id,name,body from attachment where id=:attachmentId];
        attwrap.attName = attchmnt.name;
        attwrap.attEncodedBody = EncodingUtil.base64Encode(attchmnt.body);
        return attwrap;
     
    }
    
     /**
     * Normal JSON Response
     
    public static String normalJson( Object respData ) {
        Map<String, Object> response = new Map<String, Object>();
        response.put('status', API_STATUS_NORMAL);
        if( respData != null ) response.put('data', respData);
        
        return JSON.serialize( response );
    }*/
    
   
    /*Wrapper Class which holds the Name and Body of Attachments*/
    public class AttachmentWrapper {
        public Attachment attObj{get;set;}
        public String objectType{get;set;}
        public String attEncodedBody {get; set;}
        public String attName {get; set;}
        public Boolean isSelected{get;set;}
        public Decimal sizeMb{get;set;}
    }
    
    // retrieves attachment info for table 
    public List<AttachmentWrapper> fetchAllAttachments(Set<Id> parentIdSet){
    	List <AttachmentWrapper> lstAttachmentWrapper = new List <AttachmentWrapper>();
    	for(Attachment att : [SELECT Id,Name,parentid, owner.Name, createddate, ContentType, BodyLength from Attachment where ParentId IN :parentIdSet]){
    		AttachmentWrapper wrapper = new AttachmentWrapper();
    		wrapper.attObj = att;
    		wrapper.attName = att.Name;
    		wrapper.objectType = objMap.get(att.parentId);
    		wrapper.isSelected = false;
    		wrapper.sizeMb = (Decimal)att.BodyLength / 1048576;
    		wrapper.sizeMb = wrapper.sizeMb.setScale(2);
    		lstAttachmentWrapper.add(wrapper);
    	}
    	return lstAttachmentWrapper;
    }
    
}