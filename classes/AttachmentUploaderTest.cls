/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class AttachmentUploaderTest {

    static testMethod void AttachViewPageTest() {
        // TO DO: implement unit test
        Set<Id> parentIdSet = new Set<id>();
        Test.setCurrentPage(Page.AttachmentViewPage);
        //Create an account
        Account acnt = new Account(Name='test account');
        insert acnt;
        parentIdSet.add(acnt.id);
        //create a contract
        RecordType recTyp = [select id from RecordType where sobjecttype='Contract' and developerName='Rental'];
        Contract contr = new Contract(accountid=acnt.id, recordtypeid=recTyp.id, Status='Draft');
        insert contr;
        parentIdSet.add(contr.id);
        //create an Instrument
        recTyp = [select id from RecordType where sobjecttype='Instrument__c' and developerName='Gamma'];
        Instrument__c instr = new Instrument__c(Serial_Number__c='SN-00001',Name='Test instrument', recordTypeId=recTyp.id,Contract__c=contr.id);
        insert instr;
        parentIdSet.add(instr.id);
        List<Attachment> lstAtt = attachmentfactory(parentIdSet);
        
        ApexPages.currentPage().getParameters().put('recId',instr.id);
        ApexPages.StandardController std = new ApexPages.StandardController(instr);
        AttachmentViewPageController controller = new AttachmentViewPageController(std);
        
        Test.startTest();
        controller.getAttachments();
        AttachmentViewPageController.checkAttachment(instr.id);
        AttachmentViewPageController.getAttachment(lstAtt[0].id);
        Test.stopTest();
        System.assertEquals(controller.contractId,contr.id);
        
    }
    
    /*static testMethod void ContractAttachViewPageTest() {
    	Set<Id> parentIdSet = new Set<id>();
        Test.setCurrentPage(Page.ContractAttachmentViewPage);
        //Create an account
        Account acnt = new Account(Name='test account');
        insert acnt;
        parentIdSet.add(acnt.id);
        //create a contract
        RecordType recTyp = [select id from RecordType where sobjecttype='Contract' and developerName='Rental'];
        Contract contr = new Contract(accountid=acnt.id, recordtypeid=recTyp.id, Status='Draft');
        insert contr;
        parentIdSet.add(contr.id);
        //create an Instrument
        recTyp = [select id from RecordType where sobjecttype='Instrument__c' and developerName='Gamma'];
        Instrument__c instr = new Instrument__c(Serial_Number__c='SN-00001',Name='Test instrument', recordTypeId=recTyp.id);
        insert instr;
        parentIdSet.add(instr.id);
        List<Attachment> lstAtt = attachmentfactory(parentIdSet);
        
        ApexPages.currentPage().getParameters().put('recId',contr.id);
        ApexPages.StandardController std = new ApexPages.StandardController(contr);
        ContractAttachmentViewPageController controller = new ContractAttachmentViewPageController(std);
        
        Test.startTest();
        ContractAttachmentViewPageController.checkAttachment(contr.id);
        ContractAttachmentViewPageController.getAttachment(lstAtt[0].id);
        Test.stopTest();
        System.assertEquals(controller.contractId,contr.id);
    }
    
    static testMethod void CaseAttachViewPageTest() {
        // TO DO: implement unit test
        Set<Id> parentIdSet = new Set<id>();
        Test.setCurrentPage(Page.CaseAttachmentViewPage);
        //Create an account
        Account acnt = new Account(Name='test account');
        insert acnt;
        parentIdSet.add(acnt.id);
        //create a contract
        RecordType recTyp = [select id from RecordType where sobjecttype='Contract' and developerName='Rental'];
        Contract contr = new Contract(accountid=acnt.id, recordtypeid=recTyp.id, Status='Draft');
        insert contr;
        parentIdSet.add(contr.id);
        //create an Instrument
        recTyp = [select id from RecordType where sobjecttype='Instrument__c' and developerName='Gamma'];
        Instrument__c instr = new Instrument__c(Serial_Number__c='SN-00001',Name='Test instrument', recordTypeId=recTyp.id);
        insert instr;
        parentIdSet.add(instr.id);
        //create a Case
        Case caseRec = new Case(Primary_Instrument__c=instr.id, Contract__c=contr.id, status='New', Origin='Email');
        insert caseRec;
        parentIdSet.add(caseRec.id);
        List<Attachment> lstAtt = attachmentfactory(parentIdSet);
        
        ApexPages.currentPage().getParameters().put('id',caseRec.id);
        ApexPages.StandardController std = new ApexPages.StandardController(instr);
        CaseAttachmentViewPageController controller = new CaseAttachmentViewPageController(std);
        
        Test.startTest();
        controller.getAttachments();
        CaseAttachmentViewPageController.checkAttachment(caseRec.id);
        CaseAttachmentViewPageController.getAttachment(lstAtt[0].id);
        Test.stopTest();
        System.assertEquals(controller.contractId,contr.id);
        
    }*/
    
    static List<Attachment> attachmentfactory(Set<Id> parentId){
    	List<Attachment> lstAtt = new List<Attachment>();
    	for(Id iRec : parentId){
    		Attachment aOne = new Attachment();
    		aOne.parentId = iRec;
    		aOne.body = Blob.valueOf('Do or Do not. THere is no Try');
    		aOne.ContentType = 'text/html';
    		aOne.Name = 'Test Attachment';
    		lstAtt.add(aOne);
    	}
    	insert lstAtt;
    	return lstAtt;
    }
}